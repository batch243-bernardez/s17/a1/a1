/*
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/
	
	//first function here:
	function printHelloName(){
		let firstName = prompt("Enter your first name: ");
		let lastName = prompt("Enter your last name: ");
		let age = prompt("Enter your age: ");
		let location = prompt("Enter your city address: ");

		console.log("Hello, " + firstName + " " + lastName + ".");
		console.log("You are " + age + " years old.");
		console.log("You live in " + location + " City");
	}

	printHelloName();



/*
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/

	//second function here:
	function myFavoriteBands(){
		let firstMusic = "1. Air Supply"
		let secondMusic = "2. Eraserheads"
		let thirdMusic = "3. Nsync"
		let fourthMusic = "4. Lauv"
		let fifthMusic = "5. Gloc-9"

		console.log(firstMusic);
		console.log(secondMusic);
		console.log(thirdMusic);
		console.log(fourthMusic);
		console.log(fifthMusic);
	}

	myFavoriteBands();



/*
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/
	
	//third function here:
	function favoriteMovie(){
		let firstMovie = "Iron Man"
		let rottenTomatoes = "94%"

		console.log("1. " + firstMovie);
		console.log("Rotten Tomatoes Rating: " + rottenTomatoes);
	}

	
	function favoriteMovie2(){
		let secondMovie = "Taken"
		let rottenTomatoes = "59%"

		console.log("2. " + secondMovie);
		console.log("Rotten Tomatoes Rating: " + rottenTomatoes);
	}

	function favoriteMovie3(){
		let thirdMovie = "Resident Evil"
		let rottenTomatoes = "35%"

		console.log("3. " + thirdMovie);
		console.log("Rotten Tomatoes Rating: " + rottenTomatoes);
	}

	function favoriteMovie4(){
		let fourthMovie = "Shutter Island"
		let rottenTomatoes = "68%"

		console.log("4. " + fourthMovie);
		console.log("Rotten Tomatoes Rating: " + rottenTomatoes);
	}

	function favoriteMovie5(){
		let fifthMovie = "The Bourne Identity"
		let rottenTomatoes = "84%"

		console.log("5. " + fifthMovie);
		console.log("Rotten Tomatoes Rating: " + rottenTomatoes);
	}

	favoriteMovie();
	favoriteMovie2();
	favoriteMovie3();
	favoriteMovie4();
	favoriteMovie5();


/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/

printUsers();
function printUsers(){
	alert("Hi! Please add the names of your friends.");
	let friend1 = prompt("Enter your first friend's name:"); 
	let friend2 = prompt("Enter your second friend's name:"); 
	let friend3 = prompt("Enter your third friend's name:");

	console.log("You are friends with:")
	console.log(friend1); 
	console.log(friend2); 
	console.log(friend3); 
};


/*console.log(friend1);
console.log(friend2);*/
